import db from './db'
export default {
  Query: {
    hello: () => 'world!',
    user: (parent, { id }, ctx, info) => db.user({ id }),
    users: () => db.users()
  },
  Mutation: {
    createUser: (parent, { name }, ctx, info) => db.createUser({ name }),
    deleteUser: (parent, { id }, ctx, info) => db.deleteUser({ id }),
  }
}
