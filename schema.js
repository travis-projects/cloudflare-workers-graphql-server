const gql = String.raw

export default gql`
  type Query {
    hello: String!,
    user(id: ID!): User!
    users: [User]
  }

  type Mutation {
    createUser(name: String!): User
    deleteUser(id: ID!): User
  }

  type User {
    id: ID
    name: String
  }
`
