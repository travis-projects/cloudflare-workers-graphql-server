import { GraphQLClient } from 'graphql-request'

const client = new GraphQLClient('https://eu1.prisma.sh/traverse/traverse-demo/dev')

const user = `query user ($id: ID!) {
  user (where: {
    id: $id
  }) {
    id
    name
  }
}`

const users = `query users {
  users {
    id
    name
  }
}`

const createUser =  `mutation createUser ($name: String!) {
  createUser (data: {
    name: $name
  }) {
    id
    name
  }
}`

const deleteUser = `mutation deleteUser ($id: ID!) {
  deleteUser (where: {
    id: $id
  }) {
    id
    name
  }
}`

const types = {
  user: variables => client.request(user, variables).then(({ user }) => user).catch(e => throwError(e)),
  users: () => client.request(users).then(({ users }) => users).catch(e => throwError(e)),
  createUser: variables => client.request(createUser, variables).then(({ createUser }) => createUser).catch(e => throwError(e)),
  deleteUser: variables => client.request(deleteUser, variables).then(({ deleteUser }) => deleteUser).catch(e => throwError(e))
}

const throwError = ({ message }) => { throw new Error(message) }

export default types
