# Apollo Server Cloudflare & Prisma Concept

This uses `apollo-server-cloudflare` to create a GraphQL server on Cloudflare Workers, and it also uses `graphql-request` to connect to a Prisma server.

You can try these example queries/mutations below.

### Queries

```graphql
{ hello }

query user {
  user (id: "cjsouxsvgatmn0b77sp1qn3n8") {
    name
  }
}

query users {
  users {
    id
    name
  }
}
```

### Mutations 

```graphql
mutation createUser {
  createUser (
    name: "Travis"
  ) {
    id
    name
  }
}

mutation deleteUser {
  deleteUser (id: 4) {
	name
  }
}
```
