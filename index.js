import { ApolloServer } from 'apollo-server-cloudflare'
import typeDefs from './schema'
import resolvers from './resolvers'

const server = new ApolloServer({ typeDefs, resolvers })

server.listen()
